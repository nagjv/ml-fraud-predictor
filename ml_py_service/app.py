
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.model_selection import train_test_split 
from sklearn.svm import SVC
import numpy as np 
import pandas as pd
import pymysql
from sqlalchemy import create_engine
from flask import Flask, request, jsonify, Response, json
import boto3
from config import properties

app = Flask(__name__)

@app.route('/messages', methods=['POST'])
def send_to_sqs():
    print("### Send to SQS ###")
    req_data = request.get_json(force=True)
    print(req_data)
    print("!!!!!!!!")

    session = boto3.session.Session(profile_name='sqs_write', region_name='us-east-2')
    sqs_client = session.resource('sqs')

    queue = sqs_client.get_queue_by_name(QueueName='test.fifo')

    # Send message to SQS queue
    response = queue.send_message(
        MessageBody=json.dumps(req_data),
        MessageGroupId='Fraud',
        MessageAttributes={
        'Author': {
            'DataType': 'String',
            'StringValue': 'SyncRover'
        }
    }
    )

    print(response.get('MessageId'))

    return Response(json.dumps(req_data), status=200, mimetype='application/json')

'''
{
    "account": "AAAA",
    "ach1": 0,
    "ach2": 0,
    "cc1": 0,
    "cc2": 0
}
'''
@app.route('/fraud_predictor', methods=['POST'])
def fraud_predictor():
    req_data = request.get_json()
    print("### fraud predictor ### ")
    print(req_data)

    mydb = create_engine('mysql+pymysql://' + properties['db']['user'] 
                                            + ':' + properties['db']['pwd'] 
                                            + '@' + properties['db']['host'] + ':' 
                                            + str(properties['db']['port']) + '/' 
                                            + properties['db']['schema'] , echo=False)

    dataset = pd.read_sql_query("select cc1,cc2,ach1,ach2,decision from fraud_data where decision is not null", mydb)

    array = dataset.values
    X = array[:,0:4]

    Y = array[:,4]

    validation_size = 0.25
    seed = 6
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=validation_size, random_state=seed)
    
    kn = KNeighborsClassifier()
    target_predict = "unknown"
    score = "0"
    if len(X_train) > 0:
        kn.fit(X_train, y_train) 
        
        x_new = np.array([[int(req_data['cc1']), int(req_data['cc2']), int(req_data['ach1']), int(req_data['ach2'])]])
        prediction = kn.predict(x_new) 
        
        target_predict = 'unknown' if prediction[0] is None else prediction[0]
        score = str(kn.score(X_test, y_test)*100)

        print("Predicted target value::: {}\n".format(target_predict)) 
        print("Test score: {}".format(score))

    #append reponse (prediction & confidence) to request
    req_data.update({'prediction':target_predict, 'confidence': score })

    return Response(json.dumps(req_data), status=200, mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True, port=5555)

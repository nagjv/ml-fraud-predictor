## Predictor
_Disclaimer: This is not a tutorial for machine learning or robotic process automation or socket.io or AWS SQS. This is just an example for anyone looking for simple working use case of process automation using supervised machine learning._

#### Background
This was a weekend (+1 extra vacation day) project to satisfy my machine learning & process automation curiosity. To achieve my goal of having working sample by cyber monday work day, I skipped exception handling, unit tests. You will also notice that ml service (app.py) has endpoint for posting to SQS and node service (Server.js) has endpoint to read from SQS. I created more generic simple wrapper service for SQS towards end but didn't had enough time to integrate.
ml_py_service folder has Python & scikit based machine learning flask service files. app.py also has endpoint to post messages to SQS.
node_socket_ui folder has Node + Express based UI for process automation.  Server.js also has endpoint to receive messages from SQS.

#### Tech Stack
This sample uses below tech stack.
```
Python 3.6
Node.js
Express
Socket.io
AWS SQS
MySQL (I did initial version using sqlite3)
```

#### Prerequisites
Before you clone & run this sample, make sure you have below prerequisites.
```
- Working Python 3.6 environment
- Node.js installed
- AWS account & credentials configured locally (easy, see references below)
- AWS account should have proper IAM policies to read/write to SQS (see AWS SQS section below)
- MySQL running (I had it running on a docker instance). You can use other DBs with minimum changes.
    Use script in database section to create table in your database. 
    Update ml_py_service/config.yaml & node_socket_ui/config.json with your mysql details.
```

#### Database/MySQL
cc = credit card & ach = bank account.
```sh
    CREATE TABLE `fraud_data` (
    `cc1` bigint(20) DEFAULT NULL,
    `cc2` bigint(20) DEFAULT NULL,
    `ach1` bigint(20) DEFAULT NULL,
    `ach2` bigint(20) DEFAULT NULL,
    `decision` text,
    `account` varchar(20) NOT NULL,
    PRIMARY KEY (`account`)
    )
```

#### AWS SQS
I used Fifo queue for this exercise, but that is not a requirement. You could go with normal queue as well.
Some housekeeping work to use your queue:
Update queue details in send_to_sqs() method of ml_py_service/app.py & node_socket_ui/config.json
AWS account: I do not like to provide full admin access to the AWS accounts, instead I like to provide them only require permissions. For this exercise I created two users (1) sqs_writer (2) sqs_reader. Below are their respective json polices.

```javascript
User sqs_write: only has required permission to post new message.
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sqs:GetQueueUrl",
                    "sqs:SendMessage"
                ],
                "Resource": "arn:aws:sqs:us-east-2:1234567890:test.fifo"
            }
        ]
    }
```

```javascript
User sqs_reader: only has required permission to get messages.
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sqs:DeleteMessage",
                    "sqs:ReceiveMessage",
                    "sqs:GetQueueUrl",
                    "sqs:GetQueueAttributes"
                ],
                "Resource": "arn:aws:sqs:us-east-2:1234567890:test.fifo"
            }
        ]
    }
```

#### ml_py_service
Python & scikit based machine learning flask service. Also, has endpoint to post messages to SQS.
- Run `pip install -r requirements.txt` to install dependencies.
- Run `python app.py` to start python machine learning service.
- Visit `localhost:3000` to view the app.

#### node_socket_ui
Node + Express based UI for process automation.  Also, has endpoint to receive messages from SQS.

- Run `npm install` to install dependencies.
- Run `node Server.js` to start nodejs service.
- Visit `localhost:3000` to view the app.

#### Sample requests
```javascript
POST message to queue:: localhost:5555/messages
    {
        "accid": "TEST",
        "ach1": 0,
        "ach2": 1,
        "cc1": 0,
        "cc2": 1
    }
```

```javascript
GET message from queue. When you get message from queue it would automatically push it to the UI - keep watching at localhost:3000
    localhost:3000/receive
```

```javascript
Test ML service:: localhost:5555/fraud_predictor

Request:
    {
        "accid": "TEST",
        "ach1": 0,
        "ach2": 1,
        "cc1": 0,
        "cc2": 1
    }

Response:
    {
        "accid": "TEST",
        "ach1": 0,
        "ach2": 1,
        "cc1": 0,
        "cc2": 1,
        "confidence": "57.14285714285714",
        "prediction": "Yes"
    }
```

#### References
[AWS CLI Config](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)



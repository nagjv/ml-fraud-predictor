
var express = require('express');
var app     = express();
var mysql   = require("mysql");
var http    = require('http').Server(app);
var io      = require("socket.io")(http);
var aws     = require('aws-sdk');
var Request = require("request");
var config  = require('./config.json');

console.log("config.queueURL:::"+config.queueURL);
console.log("config.mlAPI:::"+config.mlAPI);

app.set('socketIo', io);
aws.config.loadFromPath(__dirname + '/config.json');
var sqs = new aws.SQS();

// pool for DB  connections
var pool    =    mysql.createPool   ({
      host      :   config.dbHost,
      user      :   config.dbUser,
      password  :   config.dbPwd,
      database  :   config.dbSchema
});

app.get('/receive', function (req, res) {
    var params = {
        QueueUrl: config.queueURL,
        VisibilityTimeout: 5,
        MaxNumberOfMessages: 1 //can get up to 10 - for my demo use case only fetching 1
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        }
        else {
            var message = data.Messages[0],
            body = message.Body;
            json_body = JSON.parse(body);
            console.log("####SQS Message before ####"+ body);

            //request payload
            var options = {
                headers: {'content-type': 'application/json'},
                uri: config.mlAPI,
                method: 'POST',
                json: json_body
              };
              
              //call AI predictor with new event(json_body) read from SQS 
              Request(options, function (error, response, body) {
                if(error) {
                    console.log("#### Error getting prediction ####");
                    console.log(error);
                }

                console.log("#### Response#### "+response.statusCode);
                console.log(body);

                //NAG-TODO: proceed only if 200
                //if (response.statusCode == 200) {

                    //add AI new event to DB with AI decision
                    add_status(body, function(cb_res){
                        if(cb_res){
                            console.log("successfully added to DB::"+cb_res);

                        } else {
                            console.log('error adding record to DB!!');
                        }
                    });

                    //push new event to predictor UI with AI decision
                    var socket = req.app.get('socketIo');
                    socket.emit('newrow', JSON.stringify(body));

                    //remove event from SQS
                    removeFromQueue(data.Messages[0]);

                    //send response to the caller
                    res.send(body);
                    res.end();
                //}
              });
        }
    });
});

var removeFromQueue = function(message) {
    sqs.deleteMessage({
       QueueUrl: config.queueURL,
       ReceiptHandle: message.ReceiptHandle
    }, function(err, data) {
       // If errored
       err && console.log(err);
    });
 };

app.get("/",function(req,res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection',function(socket){
    console.log("A user is connected");
    socket.on('update data',function(status){
        console.log("UPDATE YES ===="+status);
        update_status(status, function(res){
            if(res){
                console.log(status);
            } else {
                console.log('error adding record!!');
            }
          });
    });

    //NAG-TODO: populate new events - need to think how this flow will work
    /*con.query('select cc1,cc2,ach1,ach2,decision from fraud_data where decision is null',function(err,rows){
        if(err) throw err;
        console.log('Data received from Db:\n');
        socket.emit('showrows', rows);
      });
      */
});

var add_status = function (status, callback) {
    console.log("adding new row::"+status);
    pool.getConnection(function(err, connection){
        if (err) {
            console.log("add_status:: DB connection err::"+err);
            callback(false);
            turn;
        }
        console.log("add_status:: DB connection successful::"+connection);
        
        //TODO: change to prepared statemet
        var stmt = "insert into nags.fraud_data values("+status.cc1+","+status.cc2+","+status.ach1+","+status.ach2+", '"+status.prediction+"', '"+status.accid+"')";
        console.log(stmt);
        connection.query(stmt, function(err,rows){
            connection.release();
            if(!err) {
              callback(true);
            }
        });
        connection.on('error', function(err) {
            console.log("add_status: DB connection on err::"+err);
            callback(false);
            return;
        });
    });
}


var update_status = function (status, callback) {
    console.log("updating decision::"+status);
    pool.getConnection(function(err, connection){
        if (err) {
            console.log("update_status:: DB connection err::"+err);
            callback(false);
            turn;
        }
        console.log("update_status:: DB connection successful::");
        var values = status.split("_");
        //how to use prepared statemets??
        var stmt = "update nags.fraud_data set decision='"+values[0]+"' where account='"+values[1]+"'";
        console.log(stmt);
        connection.query(stmt, function(err, rows){
            connection.release();
            if(!err) {
                var socket = app.get('socketIo');
                //var socket = io();
                socket.emit('refreshdata', values[1]);
                callback(true);
            }
        });
        connection.on('error', function(err) {
            console.log("update_status:: DB connection on err::"+err);
            callback(false);
            return;
        });
    });
}


http.listen(3000,function(){
    console.log("Listening on 3000");
});
